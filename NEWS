=============
Version 0.2.5
=============

* Fix the build with musl by using input_event_sec and input_event_usec.

=============
Version 0.2.4
=============

* Build system changes:
 - Add the 'introspection' and 'vapi' options, allowing to build without
   producing GObject Introspection or Vala bindings.
* Support hot-plugging in the fallback backend.
* Update the game controller mapping database.
* Various code cleanups.

=============
Version 0.2.3
=============

* Build system changes:
 - Add 'gudev' option, allowing to explicitly build without gudev.
* Fix build on Fedora 30.

=============
Version 0.2.2
=============

* Device:
 - Handle absolute axes with min > 0.
 - Handle the axes' flat to support axes to d-pad mapping.
* Mapping:
 - Support negative analog axis to button mappings.
 - Support full-range axis to button mappings.

=============
Version 0.2.1
=============

* Defer the emission of input device events by putting them into idle
  event sources.
* Update the game controller database.

=============
Version 0.2.0
=============

* Device: Add rumble support.
* Make the devices rumble in manette-test if they support it.
* Mapping:
 - Initialize the bindings to 0 before using them, avoiding improper
   values in non used fields.
 - Ensures the binding value inversion is disabled by default, avoiding
   an unexpected state.
 - Clear errno before using it to avoid string to uint16 convertions to
   fail on valid inputs.
 - Silently skip the 'platform' mapping key as it is neither useful nor
   erroneous.
* Monitor:
 - Fix the 'device-disconnected' signal marshaller.
 - Fix the devices hash table hash and comparison functions.
* Build system change:
 - Make manette-test properly depend on libmanette.
 - Change the package name from libmanette to manette.
 - Change the soname from liblibmanette.so to libmanette.so.
 - Fix the soname version.
* Fix the license header of manette-version.h.in, which was accidentaly
  not matching the one of the library.

* Bugs fixes:
 - https://gitlab.gnome.org/aplazas/libmanette/issues/1
 - https://gitlab.gnome.org/aplazas/libmanette/issues/2
 - https://gitlab.gnome.org/aplazas/libmanette/issues/3
 - https://gitlab.gnome.org/aplazas/libmanette/issues/4
 - https://gitlab.gnome.org/aplazas/libmanette/issues/5
 - https://gitlab.gnome.org/aplazas/libmanette/issues/6

=============
Version 0.1.2
=============

* Build system changes:
 - Change the package name from manette to libmanette.
 - Bump meson to 0.43.0.
 - Enable GObject Introspection.
 - Generate a VAPI file.
* Add manette_device_has_input() to let users check the capabilities of
  a gamepad. If the ManetteDevice has a mapping, it is considered to
  have the inputs defined in the mapping and only these ones, otherwise
  it has the ones returned by libevdev.
* ManetteDevice now has accessors to the user's game controller
  database, allowing to save and remove mappings for the device.
* The mapping manager now tracks the user's game controller database and
  will automatically reload the mappings in case of changes.
  ManetteMonitor tracks these changes to update the devices' mappings
  automatically.
* Add accessors for ManetteEvent's type, time, and the unfiltered or
  unmapped event values. This allows for example to build a mapping for
  any device, whether it is mapped or not.
* Various GTK-Doc fixes.
* Various precondition fixes.

=============
Version 0.1.1
=============

This is the first version of libmanette.

